## Classe de palavras 

#### Substantivo
> Subtantivo e classe de palavra que nomes as coisa e seres.
***
>> #### Substantivo Comum
>> Se refere a um grupo determinadoe de uma espécie.
>>>Ex: **Gato,Rato,mulher,homem,mesa...**
***
>> #### Substantivo Proprio
>> São nome proprios de um ser de algum grupo substantivo comum *(são nome próprios de pessoas e lugares)*.
>>> Ex: **Lucas,Maria,Eduarda,São Paulo**
***
>>#### Adjetivos 
>> E a classe de palavras que dá caracteris as coisa e seres.
>>> Ex: A bola e **redonda**
****
>>#### Pronomes 
>> São palavra que podem acompanha*(se referir)* ou subtituir o sujeito*(Pronome)* da oração.
>>> Ex: Maria saiu com o joão! Mas **ela** não ia sair com o Marcos.
***
>> #### Pronomes pessoais 
>> Ex: **Eu,tu,ele,nós,vos,eles** *(Esses são alguns)*
***
>> #### Pronomes reto.
>> Tem função de sujeito em uma oração.
>>> Ex: **Ela** foi no mercado. 
***
>> #### Pronomes Oblíquos
>> Acompanha o pronome na oração 
>>> Ex: Eu estou **contigo**
***
>> #### Pronome possessivo
>> São pronomes que manifesta posse.
>>> Ex: Aquele é **meu** carro.
***
>> #### Pronome demonstrativo.
>> São pronomes que demonstram algo *(com ideia de lugar estado tempo)*
>>> Ex: Olha **aquele** carro.
***
